#!/home4/exquisp7/python3.6/bin/python3.6
import smtplib
from email.mime.text import MIMEText
from cgi import FieldStorage
from datetime import datetime

EMAIL_USERNAME = 'contact@mx.exquisitebooths.com'
EMAIL_PASSWORD = '811W%47mMW3U'
CONTACT_EMAIL = 'billy@exquisitebooths.com'
EMAIL_SERVER = 'box5479.bluehost.com'
EMAIL_SUBJECT = 'Exquisite Booths Contact Form Submission'
EMAIL_PORT = 465
IN_DATE_FORMAT = '%a %b %d %Y'
OUT_DATE_FORMAT = '%A, %B %d, %Y'


def long_date_str(date: str, date_format: str = IN_DATE_FORMAT) -> str:
    """Formats a date string in long date format.

    Parses a date supplied in a given format and converts it to a long date
    format string.

    :param date: A string representation of a date.
    :param date_format: The format of the date string.
    :return: The supplied date in a long date format.
    """
    try:
        return datetime.strptime(date, date_format).strftime(OUT_DATE_FORMAT)
    except (TypeError, ValueError):
        return ''


def get_form_data(form: FieldStorage) -> dict:
    """Retrieves contact form field values.

    Reads contact form field values from a form into a dictionary.

    :param form: The form from which fields are to be read.
    :return: A dictionary mapping actual POST parameters to values.

    """
    data = {
        'name': form.getvalue('name'),
        'email': form.getvalue('email'),
        'phone': form.getvalue('phone'),
        'venueName': form.getvalue('venue_name'),
        'venueStreetAddress': form.getvalue('venue_address'),
        'venueCityStateZip': form.getvalue('venue_city_state_zip'),
        'eventType': form.getvalue('event_type'),
        'eventDate': long_date_str(form.getvalue('event_date')),
        'eventTime': form.getvalue('event_time'),
        'guests': form.getvalue('num_guests'),
        'comments': form.getvalue('comments')
    }
    for field in filter(lambda x: data[x] is None, data.keys()):
        data[field] = ''
    return data


def to_message(data: dict) -> MIMEText:
    """Creates an email message from contact form data.
    
    Compiles a dictionary of contact form data into a MIME email message. 
    
    :param data: The dictionary of contact form data to compile. 
    :return: A MIME email message containing the contact form data.
    """
    body = 'Name: {name}\n' \
           'E-mail Address: {email}\n' \
           'Phone Number: {phone}\n' \
           'Venue Name: {venueName}\n' \
           'Venue Address: {venueStreetAddress}\n' \
           'Venue City, State, Zip Code: {venueCityStateZip}\n' \
           'Event Type: {eventType}\n' \
           'Event Date: {eventDate}\n' \
           'Event Time: {eventTime}\n' \
           'Number of Guests: {guests}\n' \
           'Comments: {comments}'.format(**data)
    msg = MIMEText(body)
    msg['From'] = EMAIL_USERNAME
    msg['To'] = CONTACT_EMAIL
    msg['Subject'] = EMAIL_SUBJECT
    return msg


def _print_result(status: str, body: str = None) -> None:
    """Prints a result to the output stream as an HTTP response.

    Prints a result to the output stream, taking in an HTTP status line
    as the first parameter and the HTTP message body as the second parameter.
    Also prints a MIME content type header as part of the response.

    :param status: The HTTP response status code to emit.
    :param body: The HTTP response message body.
    :return: None.
    """
    print('Status: {}'.format(status))
    print('Content-Type: text/plain')
    print()
    if body is not None:
        print(body, end='')


def validate(data: dict) -> str:
    """Checks whether or not a set of data is valid for form submission.

    Performs validation checks against a set of data to determine whether or
    not it is suitable as a contact form submission. Returns a string message
    describing the validation errors found, or None if validation passes.

    :param data: The form data to validate.
    :return: A description of the errors found, or None if there are none.
    """
    if data is None:
        return 'No data found'
    required = {'name', 'email', 'phone', 'eventType'}
    if required is not None:
        for field in required:
            if field not in data or not data[field]:
                return 'Field "{}" is required but was not supplied'.format(field)
    return ''


def main() -> None:
    """Sends a contact email using submitted form data.

    Reads data submitted to the script, validates it, creates an email message using the submitted values, and sends it 
    to a contact email address. Then prints out a server response appropriate to the final status of the operation.

    :return: None. 
    """
    data = get_form_data(FieldStorage())
    errors = validate(data)
    if not errors:
        msg = to_message(data)
        server = smtplib.SMTP_SSL(EMAIL_SERVER, EMAIL_PORT)
        try:
            server.login(EMAIL_USERNAME, EMAIL_PASSWORD)
            server.sendmail(EMAIL_USERNAME, CONTACT_EMAIL, msg.as_string())
            _print_result('200 OK', 'success')
        except smtplib.SMTPException as err:
            _print_result('500 Server Error',
                          'An error occurred while contacting the server: {}\n'.format(err))
        finally:
            server.quit()
    else:
        _print_result('400 Bad Request',
                      'Invalid form submission: {}\n'.format(errors))


main()
