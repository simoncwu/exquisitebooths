﻿angular.module("Exquisite", ['ui.bootstrap', "ui.router", "ExquisiteControllers"])
.config(['$stateProvider', '$urlRouterProvider', "$locationProvider", "$urlMatcherFactoryProvider", function ($stateProvider, $urlRouterProvider, $locationProvider, $urlMatcherFactoryProvider) {
    //$locationProvider.html5Mode(true);
    $urlMatcherFactoryProvider.strictMode(false);
    $urlRouterProvider.otherwise("/");
    $stateProvider
        .state("home", {
            url: "/",
            views: {
                "": {
                    templateUrl: "/views/home/index.html",
                    controller: "HomeController"
                }
            }
        })
        .state("about", {
            url: "/about",
            views: {
                "": {
                    templateUrl: "/views/about/index.html",
                    controller: "AboutController"
                }
            }
        })
        .state("contact", {
            url: "/contact",
            views: {
                "": {
                    templateUrl: "/views/home/contact.html",
                    controller: "ContactController"
                },
                "scripts@": {
                    templateUrl: "/views/home/contact_scripts.html"
                }
            }
        })
        .state("gallery", {
            url: "/gallery",
            views: {
                "": {
                    templateUrl: "/views/gallery/index.html",
                    controller: "GalleryController"
                }
            }
        })
        .state("services", {
            url: "/services",
            views: {
                "": {
                    templateUrl: "/views/services/index.html",
                    controller: "ServicesController"
                }
            }
        })
    ;
}]);