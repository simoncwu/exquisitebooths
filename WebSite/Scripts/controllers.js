﻿angular.module("ExquisiteControllers", ["ExquisiteProviders"])
    .controller("HomeController", ["$rootScope", "$scope", "$timeout", "HomeCarouselSlidesProvider", "MetaService", function ($rootScope, $scope, $timeout, homeCarouselSlidesProvider, metaService) {
        $rootScope.pageHead = metaService.getPageHead();
        $scope.$on('$viewContentLoaded', function (event) {
            $timeout(EB.initializeUI);
        });
        $scope.interval = 4000;
        $scope.slides = homeCarouselSlidesProvider.getSlides();
    }])
    .controller("AboutController", ["$rootScope", "$scope", "$timeout", "StaffProvider", "MetaService", function ($rootScope, $scope, $timeout, staffProvider, metaService) {
        $rootScope.pageHead = metaService.getPageHead();
        $scope.$on('$viewContentLoaded', function (event) {
            $timeout(EB.initializeUI);
        });
        $scope.staff = staffProvider.getStaffInfo();
    }])
    .controller("GalleryController", ["$rootScope", "$scope", "$timeout", "MetaService", function ($rootScope, $scope, $timeout, metaService) {
        $rootScope.pageHead = metaService.getPageHead();
        $scope.$on('$viewContentLoaded', function (event) {
            $timeout(EB.initializeUI);
        });
    }])
    .controller("ContactController", ["$rootScope", "$scope", "$timeout", "EventsProvider", "MetaService", "$http", "$uibModal", function ($rootScope, $scope, $timeout, eventsProvider, metaService, $http, $uibModal) {
        $rootScope.pageHead = metaService.getPageHead();
        $scope.$on('$viewContentLoaded', function (event) {
            $timeout(EB.initializeUI);
        });
        $scope.eventTypes = eventsProvider.getEventTypes();
        $scope.formData = {};

        $scope.submitForm = function () {
            // show loading spinner
            $rootScope.loading = true;
            $rootScope.loaderMessage = "Submitting Contact Form";

            // callback for handling form submission results
            var submitCallback = function ($scope, response) {
                var success = response == "success";

                // show results modal
                $uibModal.open({
                    templateUrl: 'modal_contact_results.html',
                    controller: ["$scope", "$uibModalInstance", "message", function ($scope, $uibModalInstance, message) {
                        $scope.errorMessage = message;
                        $scope.close = function () {
                            $uibModalInstance.dismiss();
                        }
                    }],
                    resolve: {
                        message: function () {
                            return success ? null : response;
                        }
                    }
                }).opened.then(function () {
                    // hide loading spinner
                    $rootScope.loading = false;
                    $rootScope.loaderMessage = null;
                });

                // reset form
                if (success) {
                    $scope.formData = {};
                    $scope.event_date = $scope.event_time = null;
                }
            };

            var data = $scope.formData;

            // convert date and time objects to strings
            if ($scope.event_date)
                data.event_date = $scope.event_date.toDateString();
            if ($scope.event_time)
                data.event_time = $scope.event_time.toLocaleTimeString(navigator.language, { hour: '2-digit', minute: '2-digit' });

            // ajax submit the form
            $http.post("/cgi-bin/contact.py", $.param(data), {
                headers: { "Content-Type": "application/x-www-form-urlencoded;charset=utf-8;" }
            }).then(function successCallback(response) {
                submitCallback($scope, response.data);
            }, function errorCallback(response) {
                submitCallback($scope, response.statusText);
            });
        };
    }])
    .controller("ServicesController", ["$rootScope", "$scope", "$timeout", "ServicesProvider", "MetaService", function ($rootScope, $scope, $timeout, servicesProvider, metaService) {
        $rootScope.pageHead = metaService.getPageHead();
        $scope.$on('$viewContentLoaded', function (event) {
            $timeout(EB.initializeUI);
        });
        $scope.basePrice = servicesProvider.basePrice();
        $scope.optionalServices = servicesProvider.optionalServices();
    }])
;