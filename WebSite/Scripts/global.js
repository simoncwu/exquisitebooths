﻿// rename Bootstrap widgets conflicting with jQuery UI
$.fn.btn = $.fn.button.noConflict();
$.fn.tipsy = $.fn.tooltip.noConflict();

var EB = EB || {};
EB.initializeUI = function () {
    // detect external links
    $("a[href^='http://']:not([href*='exquisitebooths.com'])").on("click", function (e) {
        if (this.href) {
            e.stopPropagation();
            window.open(this.href);
            return false;
        }
    });

    // initialize jQuery UI widgets
    //$("input[type=date]").attr("type", "text").attr("pattern", "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.]\d{4}$").datepicker();
    $("select:not(.combobox)").selectmenu({
        width: "100%",
        open: function (event, ui) {
            // position to fit in viewport
            var menu = $(this).data("ui-selectmenu").menu,
                menuHeight = menu.height(),
                diff = menu[0].getBoundingClientRect().top + menuHeight - $(window).height(),
                myV = "bottom",
                atV = myV;
            if (diff > 0)
                atV = "top";
            else
                myV = "top";
            menu.position({ my: "left " + myV, at: "left " + atV, of: "[aria-owns=" + menu[0].id + "]" });

            // scroll to selected item
            var item = menu.find(".ui-state-focus");
            menu.scrollTop(item.position().top + item.height() - menuHeight + 12);
        }
    });
    $("select.combobox").combobox().attr("tabindex", "-1");

    // misc
    $("[autofocus]:first").focus();
    $(".navbar-collapse").closest("nav.navbar").click("a", function () {
        $(this).find(".navbar-collapse").collapse("hide");
    });
    $(".navbar-nav li").removeClass("active").each(function () {
        var link = $(this).children("a");
        if (link.attr("href") == document.location.hash)
            link.parent().addClass("active");
    });
};

// custom jQuery UI widgets
(function ($) {
    $.widget("ebi.combobox", {
        options: {
            input: null,
            placeholder: null
        },

        _create: function () {
            this.wrapper = $("<span>").addClass("ui-combobox").insertAfter(this.element);
            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function () {
            var selected = this.element.children(":selected"),
                value = selected.val() ? selected.text() : "";
            var wrapper = this.wrapper;
            var $input = $(this.options.input || "<input>");
            this.input = $input
                .appendTo(wrapper)
                .val(value)
                .attr("title", "")
                .attr("placeholder", this.options.placeholder)
                .addClass("ui-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left form-control")
                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: $.proxy(this, "_source"),
                    open: function (event, ui) {
                        // position to fit in viewport
                        var menu = $(this).data("ui-autocomplete").menu.element;
                        var myV, atV;
                        myV = atV = "bottom";
                        if (menu[0].getBoundingClientRect().top + menu.height() - $(window).height() > 0)
                            atV = "top";
                        else
                            myV = "top";
                        menu.position({ my: "left " + myV, at: "left " + atV, of: wrapper });
                    },
                    change: function (event, ui) {
                        $(this).trigger("input");
                        setTimeout(function () { $input.autocomplete("close"); }, 0);
                    }
                })
                .tooltip({
                    tooltipClass: "ui-state-highlight"
                });
            var required = this.element.attr("required");
            if (required) {
                this.input.attr("required", required);
                this.element.removeAttr("required");
            }
            this._on(this.input, {
                autocompleteselect: function (event, ui) {
                    ui.item.option.selected = true;
                    this._trigger("select", event, {
                        item: ui.item.option
                    });
                }
            });
        },

        _createShowAllButton: function () {
            var input = this.input,
                wasOpen = false;

            $("<a>")
                .attr("tabIndex", -1)
                .attr("title", "Show All")
                .tipsy()
                .appendTo(this.wrapper)
                .button({
                    icons: {
                        primary: "ui-icon-triangle-1-s"
                    },
                    text: false
                })
                .removeClass("ui-corner-all")
                .addClass("ui-combobox-toggle ui-corner-right")
                .mousedown(function () {
                    wasOpen = input.autocomplete("widget").is(":visible");
                })
                .click(function () {
                    input.focus();

                    // Close if already visible
                    if (wasOpen) {
                        return;
                    }

                    // Pass empty string as value to search for, displaying all results
                    input.autocomplete("search", "");
                });
        },

        _source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response(this.element.children("option").map(function () {
                var text = $(this).text();
                if (this.value && (!request.term || matcher.test(text)))
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }));
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });
})(jQuery);

$(function () {
    EB.initializeUI();
});