﻿angular.module("ExquisiteProviders", [])
    .factory("HomeCarouselSlidesProvider", function () {
        return {
            getSlides: function () {
                return [
                    {
                        id: 0,
                        imageFile: "slide-experience.png",
                        titleHead: "Are you looking",
                        titleTail: "for an experience?",
                        leadText: "Exquisite Booths specializes in integrating traditional photo booth printouts with an advanced social media sharing experience."
                    },
                    {
                        id: 1,
                        imageFile: "slide-wedding.png",
                        titleHead: "Everything from",
                        titleTail: "weddings to Bar Mitzvahs",
                        leadText: "A photo booth at your event can capture priceless photos of you and your guests, and send them home with the best favor ever!"
                    },
                    {
                        id: 2,
                        imageFile: "slide-branding.png",
                        titleHead: "Customizable",
                        titleTail: "branding and setup",
                        leadText: "Choose the backdrop that suits you best! We will work with you to create your own personal look and translate it to print."
                    }
                ];
            }
        };
    })
    .factory("StaffProvider", function () {
        return {
            getStaffInfo: function () {
                return [
                    {
                        name: "Billy",
                        title: "Owner",
                        photoFile: "billy.jpg"
                    },
                    {
                        name: "Chewy",
                        title: "Booth Technician"
                    },
                    {
                        name: "David",
                        title: "Booth Technician"
                    },
                    {
                        name: "Kenneth",
                        title: "Booth Technician"
                    },
                    {
                        name: "Crystal",
                        title: "UI Design Consultant"
                    },
                    {
                        name: "Simon",
                        title: "Technology Consultant"
                    }
                ];
            }
        }
    })
    .factory("EventsProvider", function () {
        return {
            getEventTypes: function () {
                return [
                    "Anniversary",
                    "Bar/Bat Mitzvah",
                    "Birthday",
                    "Conference",
                    "Corporate Function",
                    "Quinceañeras",
                    "Sweet 16",
                    "Wedding"
                ];
            }
        };
    })
    .factory("ServicesProvider", function () {
        var optionalServices = [
            {
                name: "Additional Hours",
                price: 100,
                unit: "/ hr"
            },
            {
                name: "Social Stands",
                description: "Allows guests to preview photos and instantly share via social media or e-mail.",
                price: 150,
                unit: "each"
            },
            {
                name: "Animated GIF Creation",
                description: "GIFs can be e-mailed in conjunction with Social Stand option; otherwise, they will be uploaded to the online gallery after 1 week.",
                price: 100
            },
            {
                name: "Custom Step and Repeat Backdrop",
                price: 300
            },
            {
                name: "Custom Branding",
                price: 250
            },
            {
                name: "White Drape Privacy Enclosure",
                price: 100
            },
            {
                name: "TV Slide Show",
                description: "Slide show loop of Photo Booth pictures on a 40\" TV.",
                price: 150
            },
            {
                name: "Instagram Printer",
                description: "Requires a reliable internet connection provided by the venue.",
                price: 350
            }
        ];
        return {
            basePrice: function () {
                return 700;
            },
            optionalServices: function () {
                return optionalServices;
            }
        };
    })
;