﻿angular.module("Exquisite")
    .service("MetaService", ["$state", function ($state) {
        return {
            getTitle: function () {
                switch ($state.$current.name) {
                    case "contact":
                        return "Contact Us";
                    case "about":
                        return "About Us";
                    case "gallery":
                        return "Photo Gallery";
                    case "services":
                        return "Our Services";
                    default:
                        return "Home";
                }
            },
            getDescription: function () {
                switch ($state.$current.name) {
                    case "contact":
                        return "Contact Exquisite Booths with a request to book our services for an upcoming event or any other inquiry regarding out services.";
                    default:
                        return "Exquisite Booths provides innovative party services for any event in New York City by organizing a fun and comfortable environment for all.";
                }
            },
            getPageHead: function () {
                return {
                    title: this.getTitle(),
                    description: this.getDescription()
                };
            }
        }
    }])
;